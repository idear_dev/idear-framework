/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.framework.response;


import com.idear.common.message.Message;
import com.idear.common.message.impl.BaseMessage;

/**
 *
 * @author CcShan
 * @date 2017/7/15
 */
public class Response<T> {

    private String respCode;

    private String respMsg;

    private T respBody;

    public Response(Message message){
        this.respCode = message.getRespCode();
        this.respMsg = message.getRespMsg();
    }

    public Response(T respBody){
        this.respBody = respBody;
        this.respCode = BaseMessage.SUCCESS.getRespCode();
        this.respMsg = BaseMessage.SUCCESS.getRespMsg();
    }

    public Response(String respCode, String respMsg){
        this.respCode = respCode;
        this.respMsg = respMsg;
    }

    public String getRespCode() {
        return respCode;
    }

    public void setRespCode(String respCode) {
        this.respCode = respCode;
    }

    public String getRespMsg() {
        return respMsg;
    }

    public void setRespMsg(String respMsg) {
        this.respMsg = respMsg;
    }

    public void setRespBody(T respBody) {
        this.respBody = respBody;
    }

    public T getRespBody() {
        return respBody;
    }

    public static final Response SUCCESS = new Response(BaseMessage.SUCCESS);

    public boolean success(){
        return BaseMessage.SUCCESS.getRespCode().equals(getRespCode());
    }
}
