/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.framework.constants;

/**
 * @author cc
 * @date 2018/7/28
 */
public class ContentType {

    /**
     * 表单类型Content-Type
     */
    public static final String CONTENT_TYPE_FORM = "application/x-www-form-urlencoded";

    /**
     * 流类型Content-Type
     */
    public static final String CONTENT_TYPE_STREAM = "application/octet-stream";

    /**
     * JSON类型Content-Type
     */
    public static final String CONTENT_TYPE_JSON = "application/json";

    /**
     * XML类型Content-Type
     */
    public static final String CONTENT_TYPE_XML = "application/xml";

    /**
     * 微信Content-Type
     */
    public static final String WECHAT_CONTENT_TYPE = "text/xml";

    /**
     * 文本类型Content-Type
     */
    public static final String CONTENT_TYPE_TEXT = "application/text";

}
