/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.framework.constants;

/**
 * @author ShanCc
 * @date 2018/7/29
 */
public class SystemHeader {

    public static final String OS_VERSION = "osVersion";

    public static final String DEVICE_ID = "deviceId";

    public static final String PHONE_MODEL = "phoneModel";

    public static final String PHONE_BRAND = "phoneBrand";

    public static final String APP_VERSION = "appVersion";

    public static final String SCREEN_WIDTH = "screenWidth";

    public static final String SCREEN_HEIGHT = "screenHeight";

    public static final String LONGITUDE = "longitude";

    public static final String LATITUDE = "latitude";

    public static final String IMSI = "imsi";

    public static final String IMEI = "imei";

    public static final String IMAC = "imac";

    public static final String IDFA = "idfa";

    public static final String CLIENT_ID = "clientId";

    public static final String APPID = "appId";

    public static final String CHANNEL = "channel";

    public static final String TOKEN = "token";

}
