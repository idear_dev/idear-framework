/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.framework.matcher;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * @author ShanCc
 * @date 2018/7/29
 */
public class WebPathMatcher {
    private static final String STAR = "*";
    private static final String SLASH = "/";

    /**
     * <p>该匹配器采用最轻量的设计，规则如下:</p>
     * <p>
     * <ul>
     * <li>1. 星号匹配（零）个或者多个字符</li>
     * <li>2. 当星号在路径分隔符中表示目录结构是，匹配一个或者多个字符。
     * 如/{@literal *}/api/getInfo.do 表示 /api之前必须有一级及以上目录</li>
     * </ul>
     * <p>
     * <pre>
     * WebPathMatcher.matches("/{@literal *}/index.html", 		            "/user/index.html");
     * WebPathMatcher.matches("{@literal *}/index.html", 			        "/user/index.html");
     * WebPathMatcher.matches("/{@literal *}/index.html", 		            "/user/index.html");
     * WebPathMatcher.matches("/user/{@literal *}/index.html", 	            "/user/api/index.html");
     * WebPathMatcher.matches("/user/{@literal *}", 				        "/user/index.html"));
     * WebPathMatcher.matches("/user{@literal *}/index.html", 	            "/user/index.html");
     * WebPathMatcher.matches("/user{@literal *}/index.html", 	            "/user/api/index.html");
     * WebPathMatcher.matches("/user/{@literal *}index.html", 				"/user/index.html");
     *
     * Above are true!!
     *
     * WebPathMatcher.matches("http://www.memedai.cn/*", 			        "http://www.memedai.cn/query.do");
     * WebPathMatcher.matches("http://www.memedai.cn*", 			        "http://www.memedai.cn.org/query.do"));
     * WebPathMatcher.matches("https://*.memedai.cn/api*", 		            "https://vip.1.memedai.cn/api/handle.do");
     * WebPathMatcher.matches("https://*.memedai.cn/api*", 		            "https://abc.def.memedai.cn/api/test/handle.do");
     * WebPathMatcher.matches("https://*.memedai.cn/api/*", 		        "https://vip.memedai.cn/api/handle.do");
     * WebPathMatcher.matches("https://*.memedai.cn/verify/{@literal *}/", 	"https://vip.memedai.cn/verify/api/handle.do");
     *
     * Above are true!!
     *
     * WebPathMatcher.matches("/user/{@literal *}/index.html", 				"/user/index.html");
     * WebPathMatcher.matches("/user/{@literal *}/index.html", 				"/index.html");
     * WebPathMatcher.matches("/user/{@literal *}/index.html", 				"/u/index.html");
     *
     * WebPathMatcher.matches("https://*.memedai.cn/api/{@literal *}/", 	"https://vip.memedai.cn/api/handle.do");
     * WebPathMatcher.matches("https://*.memedai.cn/api/{@literal *}/", 	"https://vip.memedai.cn/api");
     *
     * Above are false!!
     * </pre>
     */
    public static boolean matches(String pattern, String requestURI) {
        if (StringUtils.isAnyBlank(pattern, requestURI)) {
            return false;
        }

        /**
         * 替换空格
         */
        pattern = pattern.replaceAll("\\s*", "");

        /**
         * pattern截取开始位置
         */
        int beginOffset = 0;

        /**
         * 前星号的偏移位置
         */
        int formerStarOffset = -1;

        /**
         * 后星号的偏移位置
         */
        int latterStarOffset = -1;

        String remainingURI = requestURI;
        String prefixPattern = "";
        String suffixPattern = "";

        boolean result = false;
        do {
            formerStarOffset = StringUtils.indexOf(pattern, STAR, beginOffset);
            prefixPattern = StringUtils.substring(pattern, beginOffset,
                    formerStarOffset > -1 ? formerStarOffset : pattern.length());

            //匹配前缀Pattern
            result = remainingURI.contains(prefixPattern);
            //已经没有星号，直接返回
            if (formerStarOffset == -1) {
                return result;
            }

            //匹配失败，直接返回
            if (!result) {
                return false;
            }

            if (StringUtils.isNotEmpty(prefixPattern)) {
                remainingURI = StringUtils.substringAfter(requestURI, prefixPattern);
            }

            //匹配后缀Pattern
            latterStarOffset = StringUtils.indexOf(pattern, STAR, formerStarOffset + 1);
            suffixPattern = StringUtils.substring(pattern, formerStarOffset + 1,
                    latterStarOffset > -1 ? latterStarOffset : pattern.length());

            result = remainingURI.contains(suffixPattern);
            //匹配失败，直接返回
            if (!result) {
                return false;
            }

            if (StringUtils.isNotEmpty(suffixPattern)) {
                remainingURI = StringUtils.substringAfter(requestURI, suffixPattern);
            }

            //移动指针
            beginOffset = latterStarOffset + 1;

        } while (StringUtils.isNotBlank(suffixPattern) && StringUtils.isNotBlank(remainingURI));

        return true;
    }

    /**
     * 用于解析xml中配置的路径，用逗号分隔
     *
     * @param paths
     * @return
     */
    public static Set<String> parsePaths(String paths) {
        if (StringUtils.isBlank(paths)) {
            return new HashSet<>();
        }
        return new HashSet<>(Arrays.asList(paths.trim().split("\\s*,\\s*")));
    }

    /**
     * 判断请求URI是否与pattern内任一个匹配
     *
     * @param patterns   通配集合
     * @param requestURI 被匹配的请求URI
     * @return
     */
    public static boolean matchAny(Set<String> patterns, String requestURI) {
        if (patterns == null) {
            return false;
        }

        if (!requestURI.startsWith("/")) {
            requestURI = "/" + requestURI;
        }

        for (String pattern : patterns) {
            if (WebPathMatcher.matches(pattern, requestURI)) {
                return true;
            }
        }

        return false;
    }
}