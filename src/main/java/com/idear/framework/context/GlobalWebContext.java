/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.framework.context;

import javax.servlet.http.HttpServletRequest;

/**
 * @author ShanCc
 * @date 2018/7/29
 */
public class GlobalWebContext {


    /**
     * 应用默认相对路径（应用名称）
     */
    protected static String contextPath = "DEFAULT";

    public static String getContextPath() {
        return contextPath;
    }

    public static void setContextPath(String contextPath) {
        GlobalWebContext.contextPath = contextPath;
    }

    /**
     * 访问请求: 塞入 BodyReaderHttpServletRequestWrapper，解决request.getInputStream()/ getReader()只能读一次的问题！
     */
    protected static final ThreadLocal<HttpServletRequest> TL_REQUEST = new ThreadLocal<>();

    public static void setRequest(HttpServletRequest request) {
        TL_REQUEST.set(request);
    }

    public static HttpServletRequest getRequest() {
        return (HttpServletRequest) TL_REQUEST.get();
    }

    /**
     * 清除ThreadLocal引用(建议在每个请求线程的最后清理)
     */
    public static void clearThreadLocals() {
        TL_REQUEST.remove();
    }
}
