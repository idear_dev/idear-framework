/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.framework.filter;

import com.idear.common.util.StringUtil;
import com.idear.framework.constants.ContentType;
import com.idear.framework.context.GlobalWebContext;
import com.idear.framework.matcher.WebPathMatcher;
import com.idear.framework.network.BodyReaderHttpServletRequestWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * @author ShanCc
 * @date 2018/7/28
 */
public class ApplicationContextFilter implements Filter {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationContextFilter.class);

    public static final String PARAM_NAME_INCLUSIONS = "inclusions";
    public static final String PARAM_NAME_EXCLUSIONS = "exclusions";

    /**
     * 优先级1，优先于exclusions
     */
    private Set<String> includesPattern = new HashSet<>();

    /**
     * 优先级2
     */
    private Set<String> excludesPattern = new HashSet<>();

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        LOGGER.info("\n================= Init AppServletContextFilter Begin ================");
        String contextPath = filterConfig.getServletContext().getContextPath();
        if (StringUtil.isBlank(contextPath)) {
            contextPath = "/";
        }
        GlobalWebContext.setContextPath(contextPath);

        //需要拦截的路径
        String inclusions = filterConfig.getInitParameter(PARAM_NAME_INCLUSIONS);
        if (StringUtil.isNotBlank(inclusions)) {
            includesPattern = WebPathMatcher.parsePaths(inclusions);
        }

        //不需要拦截的路径
        String exclusions = filterConfig.getInitParameter(PARAM_NAME_EXCLUSIONS);
        if (StringUtil.isNotBlank(exclusions)) {
            excludesPattern = WebPathMatcher.parsePaths(exclusions);
        }
        LOGGER.info("\n================= Init AppServletContextFilter End ================");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws IOException, ServletException {
        LOGGER.debug("\n================= execute AppServletContextFilter ================");

        HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;
        String contentType = httpRequest.getContentType();

        if (contentType != null && contentType.contains(ContentType.CONTENT_TYPE_FORM)) {
            httpRequest.getParameterMap();
        }

        httpRequest = new BodyReaderHttpServletRequestWrapper(httpRequest);
        HttpServletResponse httpResponse = (HttpServletResponse) servletResponse;

        try {
            GlobalWebContext.setRequest(httpRequest);
            chain.doFilter(httpRequest, httpResponse);
        } finally {
            //最后清理所有的线程副本
            GlobalWebContext.clearThreadLocals();
        }
    }

    @Override
    public void destroy() {

    }
}
