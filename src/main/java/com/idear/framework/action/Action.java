/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.framework.action;


import com.idear.framework.response.Response;

/**
 *
 * @author cc
 * @date 2017/7/30
 */
public interface Action {

    /**
     * 执行方法
     * @return
     */
    Response execute();
}
