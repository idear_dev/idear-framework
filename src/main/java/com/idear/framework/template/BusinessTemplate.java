/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.framework.template;

import com.idear.common.exception.BaseException;
import com.idear.common.message.impl.BaseMessage;
import com.idear.framework.action.Action;
import com.idear.framework.response.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


/**
 *
 * @author CcShan
 * @date 2017/12/15
 */

@Component
public class BusinessTemplate {

    private Logger logger = LoggerFactory.getLogger(getClass());

    public Response execute(Action action){
        Response response;
        try{
            response = action.execute();
        }catch (Exception e){
            if (e instanceof BaseException){
                String errorCode = ((BaseException) e).getRespCode();
                String errorMsg = ((BaseException) e).getRespMsg();
                response = new Response(errorCode,errorMsg);
            }else {
                logger.error(e.getMessage());
                response = new Response(BaseMessage.SYSTEM_EXCEPTION.getRespCode(),BaseMessage.SYSTEM_EXCEPTION.getRespMsg());
            }
        }
        return response;
    }
}
