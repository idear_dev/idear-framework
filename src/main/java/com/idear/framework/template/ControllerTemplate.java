/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.framework.template;

import com.alibaba.fastjson.serializer.SerializerFeature;
import com.idear.common.exception.impl.ValidationException;
import com.idear.common.message.impl.BaseMessage;
import com.idear.common.util.JsonUtil;
import com.idear.framework.action.Action;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

/**
 *
 * @author CcShan
 * @date 2017/7/30
 */
@Component
public class ControllerTemplate {

    public String execute(Action action, BindingResult bindingResult){
        if (bindingResult != null && bindingResult.hasErrors()){
            FieldError fieldError = bindingResult.getFieldError();
            String message = fieldError.getDefaultMessage();
            if (StringUtils.isEmpty(message)){
                message = BaseMessage.ERROR_PARAM.getRespMsg();
            }
            throw new ValidationException(BaseMessage.ERROR_PARAM.getRespCode(),message);
        }
        return JsonUtil.toJsonString(action.execute(), SerializerFeature.WriteMapNullValue,SerializerFeature.WriteNullListAsEmpty,SerializerFeature.WriteNullStringAsEmpty,SerializerFeature.WriteDateUseDateFormat);
    }

    public String execute(Action action){
        return JsonUtil.toJsonString(action.execute(), SerializerFeature.WriteMapNullValue,SerializerFeature.WriteNullListAsEmpty,SerializerFeature.WriteNullStringAsEmpty,SerializerFeature.WriteDateUseDateFormat);
    }
}
