/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.framework.resolver;

import com.alibaba.fastjson.JSONException;
import com.idear.common.exception.BaseException;
import com.idear.common.exception.impl.CommunicationException;
import com.idear.common.message.impl.BaseMessage;
import com.idear.common.util.JsonUtil;
import com.idear.framework.response.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author ShanCc
 * @date 2018/1/29.
 */
public class CommonExceptionResolver extends BaseWebExceptionResolver {

    private Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * 对异常进行处理
     *
     * @param e
     * @return 响应体
     */
    @Override
    public String handleException(Exception e) {
        Response response;
        if (e instanceof BaseException){
            String errorCode = ((BaseException) e).getRespCode();
            String errorMsg = ((BaseException) e).getRespMsg();
            response = new Response(errorCode,errorMsg);
        }else if(CommunicationException.class.isInstance(e)){
            logger.error("CommunicationException",e);
            response = new Response(BaseMessage.COMMUNICATION_EXCEPTION);
        }else if (e instanceof JSONException){
            logger.error("JSONException",e);
            response = new Response(BaseMessage.PARSE_JSON_EXCEPTION);
        } else {
            logger.error("SystemException",e);
            response = new Response(BaseMessage.SYSTEM_EXCEPTION);
        }
        return JsonUtil.toJsonString(response);
    }
}
