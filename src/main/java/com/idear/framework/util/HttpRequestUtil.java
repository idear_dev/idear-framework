package com.idear.framework.util;

import com.idear.common.util.StringUtil;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by cc on 2018/7/28.
 */
public class HttpRequestUtil {

    private static final String UNKNOWN = "unknown";

    /**
     * 获取请求的真正IP
     *
     * <pre>
     *  request.getRemoteAddr() 192.168.239.196
     *  //X-Forwarded-For获取的是整个请求链路的IP
     *  request.getHeader("X-Forwarded-For") 58.63.227.162, 192.168.237.178, 192.168.238.218
     *  request.getHeader("X-Real-IP") 192.168.238.218
     * </pre>
     *
     * @param request
     * @return
     */
    public static String getRealIp(HttpServletRequest request) {
        try {
            String ip = request.getHeader("X-Forwarded-For");

            if (StringUtil.isBlank(ip) || UNKNOWN.equalsIgnoreCase(ip)) {
                ip = request.getHeader("X-Real-IP");
            }

            if (StringUtil.isBlank(ip) || UNKNOWN.equalsIgnoreCase(ip)) {
                ip = request.getHeader("Proxy-Client-IP");
            }

            if (StringUtil.isBlank(ip) || UNKNOWN.equalsIgnoreCase(ip)) {
                ip = request.getHeader("WL-Proxy-Client-IP");
            }

            return request.getRemoteAddr();
        } catch (Exception e) {
            return UNKNOWN;
        }
    }

}
