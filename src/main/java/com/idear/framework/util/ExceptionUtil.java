/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */

package com.idear.framework.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.LinkedList;
import java.util.List;

/**
 * @author ShanCc
 * @date 2018/7/29
 */
public abstract class ExceptionUtil {


    /**
     * 取得最根本的异常。
     *
     * @param throwable 受检异常
     * @return 最根本的异常。
     */
    public static Throwable getRootCause(Throwable throwable) {
        List<Throwable> causes = getCauses(throwable, true);

        return causes.isEmpty() ? null : causes.get(0);
    }

    /**
     * 取得包括当前异常在内的所有的causes异常，按出现的顺序排列。
     *
     * @param throwable 受检异常
     * @return 包括当前异常在内的所有的causes异常，按出现的顺序排列。
     */
    public static List<Throwable> getCauses(Throwable throwable) {
        return getCauses(throwable, false);
    }

    /**
     * 取得包括当前异常在内的所有的causes异常，按出现的顺序排列
     *
     * @param throwable 受检异常
     * @param reversed 是否反向
     * @return 包括当前异常在内的所有的causes异常，按出现的顺序排列。
     */
    public static List<Throwable> getCauses(Throwable throwable, boolean reversed) {
        LinkedList<Throwable> causes = new LinkedList<>();

        for (; throwable != null && !causes.contains(throwable); throwable = throwable.getCause()) {
            if (reversed) {
                causes.addFirst(throwable);
            } else {
                causes.addLast(throwable);
            }
        }

        return causes;
    }

    /**
     * 将异常转换成<code>RuntimeException</code>。
     *
     * @param exception 受检异常
     * @return to <code>RuntimeException</code
     */
    public static RuntimeException toRuntimeException(Exception exception) {
        return toRuntimeException(exception, null);
    }

    /**
     * 将异常转换成<code>RuntimeException</code>。
     *
     * @param exception 受检异常
     * @param runtimeExceptionClass 转换异常的类型
     * @return to <code>RuntimeException</code
     */
    public static RuntimeException toRuntimeException(Exception exception,
                                                      Class<? extends RuntimeException> runtimeExceptionClass) {
        if (exception == null) {
            return null;
        }

        if (exception instanceof RuntimeException) {
            return (RuntimeException) exception;
        }
        if (runtimeExceptionClass == null) {
            return new RuntimeException(exception);
        }

        RuntimeException runtimeException;

        try {
            runtimeException = runtimeExceptionClass.newInstance();
        } catch (Exception ee) {
            return new RuntimeException(exception);
        }

        runtimeException.initCause(exception);
        return runtimeException;

    }

    /**
     * 抛出Throwable，但不需要声明<code>throws Throwable</code>，区分<code>Exception</code> 或</code>Error</code>。
     *
     * @param throwable 受检异常
     * @throws Exception
     */
    public static void throwExceptionOrError(Throwable throwable) throws Exception {
        if (throwable instanceof Exception) {
            throw (Exception) throwable;
        }

        if (throwable instanceof Error) {
            throw (Error) throwable;
        }

        throw new RuntimeException(throwable); // unreachable code
    }

    /**
     * 抛出Throwable，但不需要声明<code>throws Throwable</code>，区分 <code>RuntimeException</code>、<code>Exception</code>
     * 或</code>Error</code>。
     *
     * @param throwable 受检异常
     */
    public static void throwRuntimeExceptionOrError(Throwable throwable) {
        if (throwable instanceof Error) {
            throw (Error) throwable;
        }

        if (throwable instanceof RuntimeException) {
            throw (RuntimeException) throwable;
        }

        throw new RuntimeException(throwable);
    }

    /**
     * 取得异常的stacktrace字符串。
     *
     * @param throwable 受检异常
     * @return stacktrace字符串
     */
    public static String getStackTrace(Throwable throwable) {
        return getStackTrace(throwable, -1);
    }

    /**
     * 取得异常的stacktrace字符串。
     *
     * @param throwable 受检异常
     * @param length 限定输出异常字符串的长度
     * @return stacktrace字符串
     */
    public static String getStackTrace(Throwable throwable, int length) {
        StringWriter buffer = new StringWriter();
        PrintWriter out = new PrintWriter(buffer);

        throwable.printStackTrace(out);
        out.flush();

        String traceStr = buffer.toString();
        if (length <= 0) {
            return traceStr;
        }else {
            return traceStr.substring(0, length);
        }
    }

}
