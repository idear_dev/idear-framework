/*
 * -------------------------------------------------------------------------------------
 *    Idear Confidential
 *
 *    Copyright (C) 2015 WuXi Idear Information Service Co., Ltd.
 *    All rights reserved.
 *
 *    No part of this file may be reproduced or transmitted in any form or by any means,
 *    electronic, mechanical, photocopying, recording, or otherwise, without prior
 *    written permission of WuXi Idear Information Service Co., Ltd.
 * -------------------------------------------------------------------------------------
 */
package com.idear.framework.interceptor;

import com.idear.common.exception.BaseException;
import com.idear.common.util.JsonUtil;
import com.idear.framework.constants.ContentType;
import com.idear.framework.context.GlobalWebContext;
import com.idear.framework.log.HttpLog;
import com.idear.framework.network.BodyReaderHttpServletRequestWrapper;
import com.idear.framework.util.ExceptionUtil;
import com.idear.framework.util.HttpRequestUtil;
import org.aspectj.lang.ProceedingJoinPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * @author CcShan
 * @date 2017/9/13
 */
public class HttpLogInterceptor {

    private static final Logger log = LoggerFactory.getLogger(HttpLogInterceptor.class);

    private static final String APPEND = "-APPEND-";

    /**
     * 50K的 Body打印限定长度
     * 1. 若只是纯参数的传递，除非是签名或者加解密的链接，50K应该足够的
     * 2. 超过50K的请求，基本可以确定是有图片流的上传，只打印字节流的前50K.
     */
    private static final int MAX_BODY_LEN = 50 * 1000;

    /**
     * 切面方法，记录请求应答相关信息
     *
     * @param pjp
     * @return
     * @throws Throwable
     */
    public Object watchHttp(ProceedingJoinPoint pjp) throws Throwable {
        HttpServletRequest request = GlobalWebContext.getRequest();
        if (request == null) {
            return pjp.proceed(pjp.getArgs());
        }
        long startTime = System.currentTimeMillis();
        Object responseValue = null;
        StringBuffer exceptionMsg = new StringBuffer();

        HttpLog httpLog = this.fillForm(request, exceptionMsg);

        try {
            // 执行被切入方法
            responseValue = pjp.proceed(pjp.getArgs());
        } catch (Exception e) {
            if (e instanceof BaseException){
                BaseException baseException = (BaseException)e;
                Map response = new HashMap(2);
                response.put("respCode", baseException.getRespCode());
                response.put("respMsg", baseException.getRespMsg());
                exceptionMsg.append(APPEND).append(JsonUtil.toPrettyJson(response));
            }
            throw e;
        } catch (Throwable e) {
            exceptionMsg.append(APPEND).append(ExceptionUtil.getStackTrace(e, -1));
            throw e;
        } finally {
            long endTime = System.currentTimeMillis();
            long responseTime = endTime - startTime;

            // 响应时间
            httpLog.setConsumeTime(responseTime + "ms");
            // 响应报文
            httpLog.setResponseBody(responseValue);
            httpLog.setExceptionMsg(exceptionMsg.toString());
            log.info(JsonUtil.toPrettyJson(httpLog));
        }

        return responseValue;
    }

    /**
     * 构造form
     * <p>
     * request 不能传入线程池中异步操作，它会随着ThreadLocal线程的收回而垃圾回收掉，会报空指针异常。
     *
     * @param request
     * @return
     */
    private HttpLog fillForm(final HttpServletRequest request, StringBuffer exceptionMsg) {
        HttpLog httpLog = new HttpLog();

        //分别try catch
        try {
            // Log Request Url
            httpLog.setRequestUri(request.getRequestURI());

            // Log Request Ip
            httpLog.setRequestIp(HttpRequestUtil.getRealIp(request));

        } catch (Exception e) {
            exceptionMsg.append(APPEND).append(ExceptionUtil.getStackTrace(e, -1));
        }

        try {
            // Log Request Header
            Map<String, String> requestHeaders = new TreeMap<String, String>();

            Enumeration<String> headerNames = request.getHeaderNames();
            while (headerNames.hasMoreElements()) {
                String headerName = headerNames.nextElement();
                requestHeaders.put(headerName, request.getHeader(headerName));
            }

            httpLog.setRequestHeaders(requestHeaders);

            // Log Request Parameters
            httpLog.setRequestParameters(request.getParameterMap());

        } catch (Exception e) {
            exceptionMsg.append(APPEND).append(ExceptionUtil.getStackTrace(e, -1));
        }

        // Log Request Body
        if (ContentType.CONTENT_TYPE_JSON.equals(request.getContentType()) ||
                ContentType.CONTENT_TYPE_FORM.equals(request.getContentType()) ||
                ContentType.WECHAT_CONTENT_TYPE.equals(request.getContentType())) {
            if (request instanceof BodyReaderHttpServletRequestWrapper) {
                BodyReaderHttpServletRequestWrapper bodyRequest = (BodyReaderHttpServletRequestWrapper) request;
                byte[] copyBytes = bodyRequest.getCopy();
                byte[] subBytes = copyBytes;
                try {
                    httpLog.setRequestBody(new String(subBytes, "UTF-8"));
                } catch (Exception e) {
                    exceptionMsg.append(APPEND).append(ExceptionUtil.getStackTrace(e, -1));
                }
            }
        }

        httpLog.setExceptionMsg(exceptionMsg.toString());
        return httpLog;
    }

}
